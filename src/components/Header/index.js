import "./style.css";
import logoKB from "./logoKB.png";
import Search from "../Search";

const Header = ({ showProducts }) => {
  return (
    <header>
      <figure>
        <img className="logo" alt="logo" src={logoKB} />
      </figure>
      <h1 className="title">Kenzie Burger</h1>
      <Search showProducts={showProducts} />
    </header>
  );
};

export default Header;
