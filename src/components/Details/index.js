import "./style.css";
import { FaMapMarkerAlt, FaPhoneAlt, FaFacebook } from "react-icons/fa";
import { ImSpoonKnife } from "react-icons/im";
import { AiFillInstagram } from "react-icons/ai";

const Details = () => {
  return (
    <div className="detailsContainer">
      <ul className="detailsUl">
        <li>
          <ImSpoonKnife />
          Hamburgueria
        </li>
        <li>
          <FaMapMarkerAlt />
          R. Gen. Mário Tourinho, 1733-706
        </li>
        <li>
          <FaPhoneAlt />
          (41) 99232-9867
        </li>
        <li>
          <AiFillInstagram />
          <a href="https://www.instagram.com/kenzieacademybr/?hl=pt-br">
            @kenzieburgerbr
          </a>
        </li>
        <li>
          <FaFacebook />
          <a href="https://www.facebook.com/kenzieacademybr/">
            @kenzieburgerbr
          </a>
        </li>
      </ul>
    </div>
  );
};

export default Details;
