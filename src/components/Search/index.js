import "./style.css";
import { useState } from "react";

const Search = ({ showProducts }) => {
  const [productInput, setProductInput] = useState("");

  const handleInput = () => {
    showProducts(productInput);
    setProductInput("");
  };

  return (
    <div className="inputContainer">
      <input
        type="text"
        className="inputBox"
        value={productInput}
        onChange={(event) => setProductInput(event.target.value)}
      />
      <button className="searchBtn" onClick={handleInput}>
        Pesquisar
      </button>
    </div>
  );
};

export default Search;
