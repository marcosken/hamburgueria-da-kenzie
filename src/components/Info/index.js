import "./style.css";
import duck from "./duck.png";

const Info = () => {
  return (
    <div className="infoContainer">
      <div>
        <p className="infoPhrase">
          Mate sua fome com os melhores lanches do Brasil e só pague quando
          estiver empregado!
        </p>
        <figure className="infoFigure">
          <img className="infoImg" alt="pato e hamburguer" src={duck} />
        </figure>
      </div>
    </div>
  );
};

export default Info;
