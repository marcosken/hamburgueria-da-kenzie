import Product from "../Product";

const MenuContainer = ({
  products,
  filteredProducts,
  handleClick,
  backProducts,
}) => {
  return (
    <Product
      products={products}
      handleClick={handleClick}
      filteredProducts={filteredProducts}
      backProducts={backProducts}
    />
  );
};

export default MenuContainer;
