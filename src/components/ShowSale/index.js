import "./style.css";

const ShowSale = ({
  currentSale,
  cartTotal,
  setCartTotal,
  showPreparation,
}) => {
  const handleCartTotal = () => {
    setCartTotal(
      currentSale
        .filter((item, index, array) => index === array.indexOf(item))
        .reduce((acc, item) => {
          return acc + item.price;
        }, 0)
    );
  };

  return (
    <div className="cartContainer">
      <div className="containerProductsCart">
        <h2 className="titleContainer">&lt;Meu Pedido&gt;</h2>
        {currentSale
          .filter((item, index, array) => index === array.indexOf(item))
          .map((item, index) => (
            <div className="cardProduct" key={index}>
              <h2 className="titleProduct">{item.name}</h2>
              <p className="categoryProduct">Categoria: {item.category}</p>
              <p className="priceProduct">Preço: R$ {item.price}</p>
            </div>
          ))}
      </div>
      {handleCartTotal()}
      <div className="containerCartTotal">
        <p className="pCartTotal">Total: R$ {Number(cartTotal.toFixed(2))}</p>
      </div>
      <div className="buyContainer">
        <button className="buyBtn" onClick={showPreparation}>
          Fazer Pedido
        </button>
      </div>
    </div>
  );
};

export default ShowSale;
